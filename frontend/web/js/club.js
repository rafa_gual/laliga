$(document).ready(function () {
    $("#hide_players").hide();
    $(".jugadores_club").hide();
    $("#show_players").click(function() {
        $("#hide_players").show();
        $("#show_players").hide();
        $(".jugadores_club").show();
    });
    $("#hide_players").click(function() {
        $("#hide_players").hide();
        $("#show_players").show();
        $(".jugadores_club").hide();
    });
});