$(document).ready(function () {
    $("#hide_players").hide();
    listado_clubs();
    $(".jugadores_club").hide();
    var mostrar_jugadores = false;

    $(".input_club").change(function () {
        listado_clubs();
    });

    $("#show_players").click(function () {
        $("#hide_players").show();
        $("#show_players").hide();
        mostrar_jugadores = true;
        listado_clubs();
    });
    $("#hide_players").click(function () {
        $("#hide_players").hide();
        $("#show_players").show();
        mostrar_jugadores = false;
        listado_clubs();
    });


    function listado_clubs() {
        let name_club = $("#name_club").val();
        let city_club = $("#city_club").val();
        let year_club = $("#year_club").val();

        let url_base = $("#url_base").val();
        let url_post_jugador = $("#url_post_jugador").val();
        let url_jugadores_club = $("#url_jugadores_club").val();

        $.ajax({
            method: "GET",
            url: url_base + "clubs?name="
            + name_club + '&city=' + city_club + '&year=' + year_club,
            success: function (data) {
                $("#listado_clubs").empty();
                var clubs = '';
                $.each(data, function (i, club) {
                    clubs = clubs
                        + '<tr>'
                        + '<td class="linkable" data-club_id="' + club.id + '">' + club.id + '</td>'
                        + '<td class="linkable" data-club_id="' + club.id + '">' + club.name + '</td>'
                        + '<td class="linkable" data-club_id="' + club.id + '">' + club.city + '</td>'
                        + '<td class="linkable" data-club_id="' + club.id + '">' + club.foundation_year + '</td>'
                        + '<td><a href="' + url_post_jugador
                        + club.id + '"class="btn btn-info">' + 'Crear jugador nuevo en  ' + club.name + '</a></td>'
                        + '<td class="jugadores_club">';
                    $.each(club.jugadores, function (i, jugador) {
                        clubs = clubs +
                            '<p>' + jugador.name + ' - ' + jugador.nickname + ' / ' + jugador.position + '</p>'
                    });
                    clubs = clubs + '</td></tr>';
                });
                $("#listado_clubs").append(
                    clubs);
                if (mostrar_jugadores) {
                    $(".jugadores_club").show();
                } else {
                    $(".jugadores_club").hide();
                }

                $("#listado_clubs tr").css('cursor', 'pointer');

                $("#listado_clubs .linkable").on("click", function () {
                    var url = url_jugadores_club + $(this).data("club_id");
                    window.open(url);
                });
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $("#listado_clubs").empty();
                $("#listado_clubs").append(
                    '<tr><td colspan="6"><h2>NO SE HAN ENCONTRADO CLUBS CON EL FILTRO SELECCIONADO</h2></td></tr>');
                $("#hide_players").hide();
                $("#show_players").hide();
                console.log(xhr.responseText + "\n" + xhr.status + "\n" + thrownError);
            }
        });
    }
});