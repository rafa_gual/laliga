$(document).ready(function () {
    $("#add_player").click(function () {
        post_jugadores();
    });

    function post_jugadores() {
        let name_player = $("#name_player").val();
        let nickname_player = $("#nickname_player").val();
        let position_player = $("#position_player").val();
        let id_club = $("#id_club").val();

        let url_base = $("#url_base").val();
        let url_homepage = $("#url_homepage").val();

        if (name_player == "") {
            alert("Debe introducir un nombre correto para el jugador");
            $("#name_player").focus();
            return false;
        }

        if (nickname_player == "") {
            alert("Debe introducir un nickname correto para el jugador");
            $("#nickname_player").focus();
            return false;
        }

        var player = {
            "name": name_player,
            "nickname": nickname_player,
            "position": position_player,
            "clubId": id_club,
        };

        $.ajax({
            method: "POST",
            url: url_base + "jugadores",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(player),
        })
            .done(function (msg) {
                console.log("Data Saved: " + msg);
            });

        alert("Jugador guardado correctamente");
        window.location.href = url_homepage;
    }
});