<?php

namespace AppBundle\Controller;

use AppBundle\Form\ClubsType;
use AppBundle\Form\JugadoresType;
use AppBundle\Form\JugadorPostType;
use AppBundle\Services\ApiService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Unirest;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        return $this->render('default/index.html.twig', [
        ]);
    }

    /**
     * @Route("/postjugador/{idClub}", name="postjugador")
     */
    public function postJugadorAction(Request $request, $idClub=1)
    {
        /**
         * @var ApiService
         */
        $apiService = $this->get('api.service');

        $url = $apiService->getUrl();

        $form = $this->createForm(JugadorPostType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $datosFormulario = $form->getData();

            $headers = array('Accept' => 'application/json');

            $parameters = array('name' => $datosFormulario['name'],
                'nickname' => $datosFormulario['nickname'],
                'position' => $datosFormulario['position'],
                'clubId' => $idClub
            );

            $body = Unirest\Request\Body::form($parameters);

            $response = Unirest\Request::post(
                $url . 'jugadores',
                $headers,
                $body
            );

            return $this->render('default/index.html.twig', [
            ]);
        }

        return $this->render(
            "default/jugador_post.html.twig",
            [
            'form_jugador' => $form->createView(),
            'idClub' => $idClub,
        ]
        );
    }

    /**
     * @Route("/clubs", name="clubs")
     */
    public function clubsAction(Request $request)
    {
        /**
         * @var ApiService
         */
        $apiService = $this->get('api.service');

        $url = $apiService->getUrl();

        $form = $this->createForm(ClubsType::class);

        $form->handleRequest($request);

        $headers = array('Accept' => 'application/json');
        $parameters = array();
        $response = Unirest\Request::get($url . 'clubs', $headers, $parameters);


        if ($form->isSubmitted()) {
            $datosFormulario = $form->getData();

            $parameters = array('name' => $datosFormulario['name'],
                'city' => $datosFormulario['city'],
                'year' => $datosFormulario['year']);

            $response = Unirest\Request::get($url . 'clubs', $headers, $parameters);
        }

        return $this->render(
            "default/club.html.twig",
            [
                'form_club' => $form->createView(),
                'clubs' => $response->body,
        ]
        );
    }

    /**
     * @Route("/jugadores", name="jugadores")
     */
    public function jugadoresAction(Request $request)
    {
        /**
         * @var ApiService
         */
        $apiService = $this->get('api.service');

        $url = $apiService->getUrl();

        $form = $this->createForm(JugadoresType::class);

        $form->handleRequest($request);

        $headers = array('Accept' => 'application/json');
        $parameters = array();
        $response = Unirest\Request::get($url . 'jugadores', $headers, $parameters);

        if ($form->isSubmitted()) {
            $datosFormulario = $form->getData();

            $parameters = array('name' => $datosFormulario['name'],
                'nickname' => $datosFormulario['nickname'],
                'position' => $datosFormulario['position']);

            $response = Unirest\Request::get($url . 'jugadores', $headers, $parameters);
        }

        return $this->render('default/jugador.html.twig', [
            'form_jugador' => $form->createView(),
            'jugadores' => $response->body,
        ]);
    }

    /**
     * @Route("/jugadoresclub/{idClub}", name="jugadoresclub")
     */
    public function jugadoresClubAction($idClub=1)
    {
        /**
         * @var ApiService
         */
        $apiService = $this->get('api.service');

        $url = $apiService->getUrl();

        $headers = array('Accept' => 'application/json');
        $parameters = array('club' => $idClub);
        $response = Unirest\Request::get($url. 'jugadores', $headers, $parameters);

        return $this->render("default/jugadores_club.html.twig", [
            'idClub' => $idClub,
            'jugadores' => $response->body,
        ]);
    }
}
