<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class JugadoresType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array(
                'required' => false
            ))
            ->add('nickname', TextType::class, array(
                'required' => false
            ))
            ->add('position', ChoiceType::class, array(
                'label' => 'Position: ',
                'choices' => array(
                    '' => '',
                    'portero' => 'portero',
                    'defensa' => 'defensa',
                    'medio' => 'medio',
                    'delantero' => 'delantero',
                ),
                'required' => false,
                'choices_as_values' => true,
            ))
            ->add('submit', SubmitType::class);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'app_bundle_jugadores_type';
    }
}
