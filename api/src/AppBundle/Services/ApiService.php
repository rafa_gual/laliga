<?php

namespace AppBundle\Services;

class ApiService
{
    private $apiUrl = 'http://localhost/laliga/api/web/';

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->apiUrl;
    }
}
