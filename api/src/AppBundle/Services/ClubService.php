<?php

namespace AppBundle\Services;

use AppBundle\Entity\Club;
use AppBundle\Repository\ClubRepository;
use FOS\RestBundle\Request\ParamFetcher;

class ClubService
{

    /**
     * @var ClubRepository
     */
    private $clubRepository;

    public function __construct(ClubRepository $clubRepository)
    {
        $this->clubRepository = $clubRepository;
    }

    /**
     * @param ParamFetcher $paramFetcher
     * @return array
     */
    public function findClubsFromParamQuery(ParamFetcher $paramFetcher): array
    {
        $clubName = null;
        $clubCity = null;
        $clubYear = null;

        foreach ($paramFetcher->all() as $paramKey => $paramValue) {
            switch ($paramKey) {
                case "name":
                    $clubName = $paramValue;
                    break;
                case "city":
                    $clubCity = $paramValue;
                    break;
                case "year":
                    $clubYear = $paramValue;
                    break;
            }
        }

        /**
         * @var Club[]
         */
        $clubs = $this->clubRepository
            ->findFilteredClubs($clubName, $clubCity, $clubYear);

        return $clubs;
    }

    /**
     * @return ClubRepository
     */
    public function getClubRepository(): ClubRepository
    {
        /**
         * @var ClubRepository
         */
        $ClubRepository = $this->clubRepository;
        return $ClubRepository;
    }
}
