<?php
namespace AppBundle\Services;

use AppBundle\Entity\Club;
use AppBundle\Entity\Jugador;
use AppBundle\Repository\ClubRepository;
use AppBundle\Repository\JugadorRepository;
use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Component\HttpFoundation\Request;

class JugadorService
{
    /**
     * @var JugadorRepository
     */
    private $jugadorRepository;

    /**
     * @var ClubRepository
     */
    private $clubRepository;

    public function __construct(JugadorRepository $jugadorRepository, ClubRepository $clubRepository)
    {
        $this->jugadorRepository = $jugadorRepository;
        $this->clubRepository = $clubRepository;
    }

    /**
     * @return JugadorRepository
     */
    public function getJugadorRepository(): JugadorRepository
    {
        /**
         * @var JugadorRepository
         */
        $jugadorRepository = $this->jugadorRepository;

        return $jugadorRepository;
    }

    /**
     * @param ParamFetcher $paramFetcher
     * @return array
     */
    public function findJugadoresFromParamQuery(ParamFetcher $paramFetcher): array
    {
        $jugadorName = null;
        $jugadorNickname = null;
        $jugadorPosition = null;
        $clubId = null;

        foreach ($paramFetcher->all() as $paramKey => $paramValue) {
            switch ($paramKey) {
                case "name":
                    $jugadorName = $paramValue;
                    break;
                case "nickname":
                    $jugadorNickname = $paramValue;
                    break;
                case "position":
                    $jugadorPosition = $paramValue;
                    break;
                case "club":
                    $clubId = $paramValue;
                    break;
            }
        }

        /**
         * @var Jugador[]
         */
        $jugadores = $this->jugadorRepository
            ->findFilteredJugadores($jugadorName, $jugadorNickname, $jugadorPosition, $clubId);

        return $jugadores;
    }

    public function getJugadorFromRequest(Request $request)
    {
        $name = $request->request->get('name');
        $nickname = $request->request->get('nickname');
        $position = $request->request->get('position');
        $clubId = $request->request->get('clubId');

        /**
         * @var Club
         */
        $club = $this->clubRepository->findClubById($clubId);

        /**
         * @var Jugador
         */
        $jugador = new Jugador();
        $jugador->setName($name);
        $jugador->setNickname($nickname);
        $jugador->setPosition($position);
        $jugador->setClub($club);

        return $jugador;
    }
}
