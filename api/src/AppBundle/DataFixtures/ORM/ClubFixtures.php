<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Club;

class ClubFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for ($index = 1; $index <= 50; $index++) {
            $club = new Club();
            $club->setId($index);
            $club->setName('club '. ($index));
            $club->setCity('city '. ($index));
            $club->setFoundationYear(1900 + $index);
            $manager->persist($club);
        }

        $manager->flush();
    }
}
