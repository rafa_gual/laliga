<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Club;
use AppBundle\Entity\Jugador;
use AppBundle\Repository\ClubRepository;
use AppBundle\Repository\JugadorRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class JugadorFixtures extends Fixture implements DependentFixtureInterface, ContainerAwareInterface
{
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }


    public function load(ObjectManager $manager)
    {
        /**
         * @var ClubRepository
         */
        $clubRepository = $this->container->get('club.repository');

        /**
         * @var JugadorRepository
         */
        $jugadorRepository = $this->container->get('jugador.repository');

        $positions = $jugadorRepository->getPositions();


        for ($index = 1; $index <= 200; $index++) {
            /**
             * @var Club
             */
            $club = $clubRepository->findClubById(($index-1)%50 + 1);

            $jugador = new Jugador();
            $jugador->setId($index);
            $jugador->setClub($club);
            $jugador->setName('jugador '. ($index));
            $jugador->setNickName('nick '. ($index));
            $jugador->setPosition($positions [ ($index-1)%4 ]);
            $manager->persist($jugador);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            ClubFixtures::class,
        );
    }
}
