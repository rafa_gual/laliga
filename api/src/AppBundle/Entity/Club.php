<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Club
 *
 * @ORM\Table(name="club")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ClubRepository")
 */
class Club
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(name="city", type="string", length=255, nullable=true)
     */
    private $city;

    /**
     * @var int|null
     *
     * @ORM\Column(name="foundationYear", type="integer", nullable=true)
     */
    private $foundationYear;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Jugador", mappedBy="club")
     *
     */
    private $jugadores;

    public function __construct()
    {
        $this->jugadores = new ArrayCollection();
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Club
     */
    public function setName(string $name): Club
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set city.
     *
     * @param string $city
     *
     * @return Club
     */
    public function setCity(string $city) :Club
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city.
     *
     * @return string|null
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set foundationYear.
     *
     * @param int $foundationYear
     *
     * @return Club
     */
    public function setFoundationYear(int $foundationYear): Club
    {
        $this->foundationYear = $foundationYear;

        return $this;
    }

    /**
     * Get foundationYear.
     *
     * @return int|null
     */
    public function getFoundationYear()
    {
        return $this->foundationYear;
    }

    /**
     * Add jugador.
     *
     * @param Jugador $jugadores
     *
     * @return Club
     */
    public function addJugador(Jugador $jugadores): Club
    {
        $this->jugadores[] = $jugadores;

        return $this;
    }

    /**
     * Get jugadores.
     *
     * @return Collection|null
     */
    public function getJugadores()
    {
        return $this->jugadores;
    }
}
