<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Jugador;
use Doctrine\ORM\EntityRepository;

/**
 * Class JugadorRepository
 * @package AppBundle\Repository
 */
class JugadorRepository extends EntityRepository
{
    private $positions = ['portero', 'defensa', 'medio', 'delantero'];

    /**
     * Return several Jugadores filtered by name, nickname, cluh or position
     * Asume that is a like filter and returns all matches club plus name plus nickname plus position
     *
     * @param string|null $jugadorName
     * @param string|null $jugadorNickname
     * @param string|null $jugadorPosition
     * @param string|null $jugadorClubId
     * @return array
     */
    public function findFilteredJugadores(
        string $jugadorName = null,
        string $jugadorNickname = null,
        string $jugadorPosition = null,
        string $jugadorClubId = null
    ): array {
        $dql = "SELECT j, c FROM AppBundle:Jugador j
                 JOIN j.club c";
        if ($jugadorName) {
            $dql .= " WHERE j.name LIKE :name";
        }
        if ($jugadorNickname) {
            $dql .= ($jugadorName) ? " OR " : " WHERE ";
            $dql .= " j.nickname LIKE :nick";
        }
        if ($jugadorPosition) {
            $dql .= ($jugadorName || $jugadorNickname) ? " OR " : " WHERE ";
            $dql .= 'j.position = :position';
        }
        if ($jugadorClubId) {
            $dql .= ($jugadorName || $jugadorNickname || $jugadorPosition) ? " OR " : " WHERE ";
            $dql .= 'c.id = :club';
        }

        $dql .= ' ORDER BY j.id ASC';

        $query = $this->_em->createQuery($dql);
        if ($jugadorName) {
            $query->setParameter('name', '%' . $jugadorName . '%');
        }
        if ($jugadorNickname) {
            $query->setParameter('nick', '%' . $jugadorNickname . '%');
        }
        if ($jugadorPosition) {
            $query->setParameter('position', $jugadorPosition);
        }
        if ($jugadorClubId) {
            $query->setParameter('club', $jugadorClubId);
        }
        return $query->getResult();
    }

    /**
     * Find a Jugador by the id
     *
     * @param int $id
     * @return Jugador|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findJugadorById(int $id): ?Jugador
    {
        $dql = "SELECT j FROM AppBundle:Jugador j
                WHERE j.id = :id";

        $query = $this->_em->createQuery($dql);
        $query->setParameter('id', $id);

        return $query->getOneOrNullResult();
    }

    /**
     * Save a Jugador
     *
     * @param Jugador $jugador
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function saveJugador(Jugador $jugador)
    {
        $this->_em->persist($jugador);
        $this->_em->flush();
    }

    /**
     * @return array
     */
    public function getPositions(): array
    {
        return $this->positions;
    }
}
