<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Club;
use Doctrine\ORM\EntityRepository;

/**
 * Class ClubRepository
 * @package AppBundle\Repository
 */
class ClubRepository extends EntityRepository
{
    /**
     * Return several Clubs filtered by name, city or foundation year
     * Asume that is a like filter and returns all matches name plus city plus year
     *
     * @param string|null $clubName
     * @param string|null $clubCity
     * @param string|null $clubYear
     * @return array
     */
    public function findFilteredClubs(string $clubName = null, string $clubCity = null, string $clubYear = null): array
    {
        $dql = "SELECT c FROM AppBundle:Club c";
        if ($clubName) {
            $dql .= " WHERE c.name LIKE :name";
        }
        if ($clubCity) {
            $dql .= ($clubName) ? " OR " : " WHERE ";
            $dql .= " c.city LIKE :city";
        }
        if ($clubYear) {
            $dql .= ($clubName || $clubCity) ? " OR " : " WHERE ";
            $dql .= 'c.foundationYear LIKE :year';
        }

        $dql .= ' ORDER BY c.name ASC';

        $query = $this->_em->createQuery($dql);
        if ($clubName) {
            $query->setParameter('name', '%' . $clubName . '%');
        }
        if ($clubCity) {
            $query->setParameter('city', '%' . $clubCity . '%');
        }
        if ($clubYear) {
            $query->setParameter('year', '%' . $clubYear . '%');
        }

        return $query->getResult();
    }

    /**
     * Find a Club by the id
     *
     * @param int $id
     * @return Club|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findClubById(int $id): ?Club
    {
        $dql = "SELECT c FROM AppBundle:Club c
                WHERE c.id = :id";

        $query = $this->_em->createQuery($dql);
        $query->setParameter('id', $id);

        return $query->getOneOrNullResult();
    }
}
