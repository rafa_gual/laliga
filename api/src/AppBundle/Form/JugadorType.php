<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class JugadorType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'description' => 'Player name',
            ])
            ->add('nickname', TextType::class, [
                'description' => 'Nickname that idetifies the player',
            ])
            ->add('position', TextType::class, [
                'description' => 'Preference player position: \'portero\', \'defensa\', \'medio\', \'delantero\'',
            ])
            ->add('club', TextType::class, [
                'description' => 'Club identifier',
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AppBundle\Entity\Jugador',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_jugador';
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'jugador';
    }
}
