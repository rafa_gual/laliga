<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Jugador;
use AppBundle\Services\JugadorService;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Routing\ClassResourceInterface;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use FOS\RestBundle\View\View;
use FOS\RestBundle\View\RouteRedirectView;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use Symfony\Component\HttpFoundation\Response;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

/**
 * Class JugadorController
 * @package AppBundle\Controller
 *
 * @RouteResource("jugadore")
 */
class JugadorController extends FOSRestController implements ClassResourceInterface
{
    /**
     *  Get a collection of Jugadores
     *
     * @QueryParam(name="name", nullable=true, description="Part of Jugador Name")
     * @QueryParam(name="nickname", nullable=true, description="Part of Jugador Nickname")
     * @QueryParam(name="position", nullable=true, description="jugador Position")
     * @QueryParam(name="club", nullable=true, description="club id")
     *
     * @ApiDoc(
     *     description="Returns a collection of Jugadores",
     *     section="jugadores",
     *     statusCodes={
     *        200="Returned when successful",
     *        404="Not found"
     *      }
     * )
     *
     * @param ParamFetcher $paramFetcher
     *
     * @return View
     */
    public function cgetAction(ParamFetcher $paramFetcher): View
    {
        /**
         * @var JugadorService
         */
        $jugadorService = $this->get('jugador.service');

        /**
         * @var Jugador[]
         */
        $jugadores = $jugadorService->findJugadoresFromParamQuery($paramFetcher);

        if ($jugadores == null) {
            return new View(null, Response::HTTP_NOT_FOUND);
        }

        return new View($jugadores, Response::HTTP_OK);
    }

    /**
     * Post a new jugador
     *
     * @ApiDoc(
     *     decription="Create a new Jugador",
     *     section="jugadores",
     *     input="AppBundle\Form\JugadoresType",
     *     requirements={
     *      {"name"="name", "dataType"="string", "required"=true, "description"="Jugador name"},
     *      {"name"="nickname", "dataType"="string", "required"=true, "description"="Jugador nickname"},
     *      {"name"="position", "dataType"="string", "required"=true, "description"="Jugador position: portero, defensa, medio or delantero"},
     *      {"name"="club", "dataType"="string", "required"=true, "description"="Club id"}
     *      },
     *     statusCodes={
     *         201 = "Returned when a new BJugadorlogPost has been successful created",
     *         400 = "Return when data errors",
     *         404 = "Return when not found",
     *         500 = "Return when internal errors"
     *     }
     * )
     *
     * @param Request $request
     * @return View|\Symfony\Component\Form\FormInterface
     */
    public function postAction(Request $request): object
    {
        /**
         * @var JugadorService
         */
        $jugadorService = $this->get('jugador.service');
        $jugadorRepository =$jugadorService->getJugadorRepository();

        /**
         * @var Jugador
         */
        $jugador = $jugadorService->getJugadorFromRequest($request);

        $jugadorRepository->saveJugador($jugador);

        $routeOptions = [
            'id' => $jugador->getId(),
            '_format' => $request->get('_format'),
        ];

        return $this->routeRedirectView('get_jugadores', $routeOptions, Response::HTTP_CREATED);
    }
}
