<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Club;
use AppBundle\Services\ClubService;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Routing\ClassResourceInterface;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use Symfony\Component\HttpFoundation\Response;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

/**
 * Class ClubController
 * @package AppBundle\Controller
 *
 * @RouteResource("club")
 */
class ClubController extends FOSRestController implements ClassResourceInterface
{

    /**
     *  Get a collection of Clubs
     *
     * @QueryParam(name="name", nullable=true, description="Part of Club Name")
     * @QueryParam(name="city", nullable=true, description="Part of Club City")
     * @QueryParam(name="year", nullable=true, description="Club Foundation year")
     *
     * @ApiDoc(
     *     description="Returns a collection of Clubs",
     *     section="clubs",
     *     statusCodes={
     *        200="Returned when successful",
     *        404="Not found"
     *      }
     * )
     *
     * @param ParamFetcher $paramFetcher
     *
     * @return View
     */
    public function cgetAction(ParamFetcher $paramFetcher): View
    {
        /**
         * @var ClubService
         */
        $clubService = $this->get('club.service');

        /**
         * @var Club[]
         */
        $clubs = $clubService->findClubsFromParamQuery($paramFetcher);

        if ($clubs == null) {
            return new View(null, Response::HTTP_NOT_FOUND);
        }

        return new View($clubs, Response::HTTP_OK);
    }
}
