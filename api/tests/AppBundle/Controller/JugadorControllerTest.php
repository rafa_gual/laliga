<?php

namespace AppBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use GuzzleHttp\Client;
use Symfony\Component\Console\Input\StringInput;
use GuzzleHttp\Exception\RequestException;

class JugadorControllerTest extends WebTestCase
{
    protected static $application;
    protected $clientGuzzle;
    protected $client;

    public function setUp(): void
    {
        self::runCommand('app:fixtures:reload');

        $this->client = static::createClient();
        $container = $this->client->getContainer();
        $apiService = $container->get('api.service');
        $url = $apiService->getUrl();

        $this->clientGuzzle = new Client(
            ['base_uri' => $url]
        );
    }

    protected static function runCommand($command)
    {
        $command = sprintf('%s --quiet', $command);

        return self::getApplication()->run(new StringInput($command));
    }

    protected static function getApplication()
    {
        if (null === self::$application) {
            $client = static::createClient();

            self::$application = new Application($client->getKernel());
            self::$application->setAutoExit(false);
        }

        return self::$application;
    }

    public function testGetAllJugadores()
    {
        $jugadores = null;
        $jugador = null;

        try {
            $response = $this->clientGuzzle->get('jugadores');
            $jugadores = json_decode($response->getBody());
            $jugador = $jugadores[12];
            $club = $jugador->club;
            $otrosJugadores = $club->jugadores;
        } catch (RequestException $e) {
            $response = $e->getResponse();
        }
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals('OK', $response->getReasonPhrase());
        $this->assertEquals('application/json', $response->getHeaders()["Content-Type"][0]);
        $this->assertEquals(200, count($jugadores));
        $this->assertObjectHasAttribute('name', $jugador);
        $this->assertEquals('jugador 13', $jugador->name);
        $this->assertObjectHasAttribute('nickname', $jugador);
        $this->assertEquals('nick 13', $jugador->nickname);
        $this->assertObjectHasAttribute('position', $jugador);
        $this->assertEquals('portero', $jugador->position);
        $this->assertObjectHasAttribute('club', $jugador);
        $this->assertEquals('club 13', $club->name);
        $this->assertEquals(3, count($otrosJugadores));
    }

    public function testGetJugadoresByName()
    {
        $jugadores = null;
        $jugador = null;

        try {
            $response = $this->clientGuzzle->get('jugadores?name=3');
            $jugadores = json_decode($response->getBody());
            $jugador = $jugadores[5];
        } catch (RequestException $e) {
            $response = $e->getResponse();
        }

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals('OK', $response->getReasonPhrase());
        $this->assertEquals('application/json', $response->getHeaders()["Content-Type"][0]);
        $this->assertEquals(38, count($jugadores));
        $this->assertEquals('jugador 32', $jugador->name);
        $this->assertEquals('nick 32', $jugador->nickname);
        $this->assertEquals('delantero', $jugador->position);
        $this->assertEquals(32, $jugador->club->id);
    }

    public function testGetJugadoresByNickname()
    {
        $jugadores = null;
        $jugador = null;

        try {
            $response = $this->clientGuzzle->get('jugadores?nickname=6');
            $jugadores = json_decode($response->getBody());
            $jugador = $jugadores[7];
        } catch (RequestException $e) {
            $response = $e->getResponse();
        }

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals('OK', $response->getReasonPhrase());
        $this->assertEquals('application/json', $response->getHeaders()["Content-Type"][0]);
        $this->assertEquals(38, count($jugadores));
        $this->assertEquals('jugador 61', $jugador->name);
        $this->assertEquals('nick 61', $jugador->nickname);
        $this->assertEquals('portero', $jugador->position);
        $this->assertEquals(11, $jugador->club->id);
    }

    public function testGetJugadoresByClubId()
    {
        $jugadores = null;
        $jugador = null;

        try {
            $response = $this->clientGuzzle->get('jugadores?club=8');
            $jugadores = json_decode($response->getBody());
            $jugador = $jugadores[2];
        } catch (RequestException $e) {
            $response = $e->getResponse();
        }

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals('OK', $response->getReasonPhrase());
        $this->assertEquals('application/json', $response->getHeaders()["Content-Type"][0]);
        $this->assertEquals(4, count($jugadores));
        $this->assertEquals('jugador 108', $jugador->name);
        $this->assertEquals('nick 108', $jugador->nickname);
        $this->assertEquals('delantero', $jugador->position);
        $this->assertEquals(8, $jugador->club->id);
    }

    public function testGetJugadoresByNameOrClubId()
    {
        $jugadores = null;
        $jugador = null;

        try {
            $response = $this->clientGuzzle->get('jugadores?name=jugador 13&club=8');
            $jugadores = json_decode($response->getBody());
            $jugador = $jugadores[2];
        } catch (RequestException $e) {
            $response = $e->getResponse();
        }

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals('OK', $response->getReasonPhrase());
        $this->assertEquals('application/json', $response->getHeaders()["Content-Type"][0]);
        $this->assertEquals(15, count($jugadores));
        $this->assertEquals('jugador 58', $jugador->name);
        $this->assertEquals('nick 58', $jugador->nickname);
        $this->assertEquals('defensa', $jugador->position);
        $this->assertEquals(8, $jugador->club->id);
    }

    public function testGetJugadoresWithNoExistantName()
    {
        $jugadores = null;

        try {
            $response = $this->clientGuzzle->get('jugadores?name=test');
            $jugadores = json_decode($response->getBody());
        } catch (RequestException $e) {
            $response = $e->getResponse();
        }

        $this->assertEquals(404, $response->getStatusCode());
        $this->assertEquals('Not Found', $response->getReasonPhrase());
        $this->assertEquals('application/json', $response->getHeaders()["Content-Type"][0]);
    }

    public function testPostJugador()
    {

        //POST
        $dataJson = [
            'name' => 'jugador post',
            'nickname' => 'nick post',
            'position' => 'portero',
            'clubId' => '3'
        ];

        try {
            $response = $this->clientGuzzle->post(
                'jugadores',
                [ 'json' =>
                    $dataJson
                ]
            );
        } catch (RequestException $e) {
            $response = $e->getResponse();
        }

        $this->assertEquals(201, $response->getStatusCode());
        $this->assertEquals('Created', $response->getReasonPhrase());
        $this->assertEquals('application/json', $response->getHeaders()["Content-Type"][0]);

        //Tras el POST
        $jugadores = null;
        $jugador = null;

        try {
            $response = $this->clientGuzzle->get('jugadores');
            $jugadores = json_decode($response->getBody());
            $jugador = $jugadores[200];
        } catch (RequestException $e) {
            $response = $e->getResponse();
        }

        $this->assertEquals(201, count($jugadores));
        $this->assertEquals('jugador post', $jugador->name);
        $this->assertEquals('nick post', $jugador->nickname);
        $this->assertEquals('portero', $jugador->position);
        $this->assertEquals('club 3', $jugador->club->name);
    }

    public function testPostWithNoNameJugador()
    {
        //POST
        $dataJson = [
            'nickname' => 'nick post2',
            'position' => 'defensa',
            'clubId' => '5'
        ];

        try {
            $response = $this->clientGuzzle->post(
                'jugadores',
                ['json' =>
                    $dataJson
                ]
            );
        } catch (RequestException $e) {
            $response = $e->getResponse();
        }

        $this->assertEquals(500, $response->getStatusCode());
        $this->assertEquals('Internal Server Error', $response->getReasonPhrase());
        $this->assertEquals('application/json', $response->getHeaders()["Content-Type"][0]);

        //Tras el POST
        $jugadores = null;

        try {
            $response = $this->clientGuzzle->get('jugadores');
            $jugadores = json_decode($response->getBody());
        } catch (RequestException $e) {
            $response = $e->getResponse();
        }

        $this->assertEquals(200, count($jugadores));
    }

    public function testPostWtihNoExistantClub()
    {
        //POST
        $dataJson = [
            'name' => 'jugador post',
            'nickname' => 'nick post2',
            'position' => 'defensa',
            'clubId' => '250'
        ];

        try {
            $response = $this->clientGuzzle->post(
                'jugadores',
                ['json' =>
                    $dataJson
                ]
            );
        } catch (RequestException $e) {
            $response = $e->getResponse();
        }

        $this->assertEquals(500, $response->getStatusCode());
        $this->assertEquals('Internal Server Error', $response->getReasonPhrase());
        $this->assertEquals('application/json', $response->getHeaders()["Content-Type"][0]);

        //Tras el POST
        $jugadores = null;

        try {
            $response = $this->clientGuzzle->get('jugadores');
            $jugadores = json_decode($response->getBody());
        } catch (RequestException $e) {
            $response = $e->getResponse();
        }

        $this->assertEquals(200, count($jugadores));
    }
}
