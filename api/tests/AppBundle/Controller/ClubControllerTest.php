<?php

namespace AppBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use GuzzleHttp\Client;
use Symfony\Component\Console\Input\StringInput;
use GuzzleHttp\Exception\RequestException;

class ClubControllerTest extends WebTestCase
{
    protected static $application;
    protected $clientGuzzle;
    protected $client;

    public function setUp(): void
    {
        self::runCommand('app:fixtures:reload');

        $this->client = static::createClient();
        $container = $this->client->getContainer();
        $apiService = $container->get('api.service');
        $url = $apiService->getUrl();

        $this->clientGuzzle = new Client(
            ['base_uri' => $url]
        );
    }

    protected static function runCommand($command)
    {
        $command = sprintf('%s --quiet', $command);

        return self::getApplication()->run(new StringInput($command));
    }

    protected static function getApplication()
    {
        if (null === self::$application) {
            $client = static::createClient();

            self::$application = new Application($client->getKernel());
            self::$application->setAutoExit(false);
        }

        return self::$application;
    }

    public function testGetAllClubs()
    {
        $clubs = null;
        $club = null;

        try {
            $response = $this->clientGuzzle->get('clubs');
            $clubs = json_decode($response->getBody());
            $club = $clubs[3];
            $jugadores = $club->jugadores;
            $jugador = $jugadores[3];
        } catch (RequestException $e) {
            $response = $e->getResponse();
        }

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals('OK', $response->getReasonPhrase());
        $this->assertEquals('application/json', $response->getHeaders()["Content-Type"][0]);
        $this->assertEquals(50, count($clubs));
        $this->assertObjectHasAttribute('name', $club);
        $this->assertEquals('club 12', $club->name);
        $this->assertObjectHasAttribute('city', $club);
        $this->assertEquals('city 12', $club->city);
        $this->assertObjectHasAttribute('foundation_year', $club);
        $this->assertEquals('1912', $club->foundation_year);
        $this->assertEquals(4, count($jugadores));
        $this->assertEquals('jugador 162', $jugador->name);
    }

    public function testGetClubsByName()
    {
        $clubs = null;
        $club = null;

        try {
            $response = $this->clientGuzzle->get('clubs?name=3');
            $clubs = json_decode($response->getBody());
            $club = $clubs[2];
        } catch (RequestException $e) {
            $response = $e->getResponse();
        }

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals('OK', $response->getReasonPhrase());
        $this->assertEquals('application/json', $response->getHeaders()["Content-Type"][0]);
        $this->assertEquals(14, count($clubs));
        $this->assertObjectHasAttribute('name', $club);
        $this->assertEquals('club 3', $club->name);
        $this->assertObjectHasAttribute('city', $club);
        $this->assertEquals('city 3', $club->city);
        $this->assertObjectHasAttribute('foundation_year', $club);
        $this->assertEquals('1903', $club->foundation_year);
    }

    public function testGetClubsByCity()
    {
        $clubs = null;
        $club = null;

        try {
            $response = $this->clientGuzzle->get('clubs?city=2');
            $clubs = json_decode($response->getBody());
            $club = $clubs[1];
        } catch (RequestException $e) {
            $response = $e->getResponse();
        }
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals('OK', $response->getReasonPhrase());
        $this->assertEquals('application/json', $response->getHeaders()["Content-Type"][0]);
        $this->assertEquals(14, count($clubs));
        $this->assertObjectHasAttribute('name', $club);
        $this->assertEquals('club 2', $club->name);
        $this->assertObjectHasAttribute('city', $club);
        $this->assertEquals('city 2', $club->city);
        $this->assertObjectHasAttribute('foundation_year', $club);
        $this->assertEquals('1902', $club->foundation_year);
    }

    public function testGetClubsByYear()
    {
        $clubs = null;
        $club = null;

        try {
            $response = $this->clientGuzzle->get('clubs?year=4');
            $clubs = json_decode($response->getBody());
            $club = $clubs[1];
        } catch (RequestException $e) {
            $response = $e->getResponse();
        }
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals('OK', $response->getReasonPhrase());
        $this->assertEquals('application/json', $response->getHeaders()["Content-Type"][0]);
        $this->assertEquals(14, count($clubs));
        $this->assertObjectHasAttribute('name', $club);
        $this->assertEquals('club 24', $club->name);
        $this->assertObjectHasAttribute('city', $club);
        $this->assertEquals('city 24', $club->city);
        $this->assertObjectHasAttribute('foundation_year', $club);
        $this->assertEquals('1924', $club->foundation_year);
    }

    public function testGetClubsByNameOrCityOrYear()
    {
        $clubs = null;
        $club = null;

        try {
            $response = $this->clientGuzzle->get('clubs?city=22&name=b 1&year=05');
            $clubs = json_decode($response->getBody());
            $club = $clubs[12];
        } catch (RequestException $e) {
            $response = $e->getResponse();
        }
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals('OK', $response->getReasonPhrase());
        $this->assertEquals('application/json', $response->getHeaders()["Content-Type"][0]);
        $this->assertEquals(13, count($clubs));
        $this->assertObjectHasAttribute('name', $club);
        $this->assertEquals('club 5', $club->name);
        $this->assertObjectHasAttribute('city', $club);
        $this->assertEquals('city 5', $club->city);
        $this->assertObjectHasAttribute('foundation_year', $club);
        $this->assertEquals('1905', $club->foundation_year);
    }

    public function testGetClubsWithNoExistantName()
    {
        $clubs = null;
        $club = null;

        try {
            $response = $this->clientGuzzle->get('clubs?name=test');
            $clubs = json_decode($response->getBody());
        } catch (RequestException $e) {
            $response = $e->getResponse();
        }

        $this->assertEquals(404, $response->getStatusCode());
        $this->assertEquals('Not Found', $response->getReasonPhrase());
        $this->assertEquals('application/json', $response->getHeaders()["Content-Type"][0]);
    }
}
